[![pipeline status](https://gitlab.com/tum-vision/lie-spline-experiments/badges/master/pipeline.svg)](https://gitlab.com/tum-vision/lie-spline-experiments/commits/master)

## Lie Group Cumulative B-spline Experiments

This project contains the code for experiments described in the paper:

* **Efficient Derivative Computation for Cumulative B-Splines on Lie Groups**, C. Sommer, V. Usenko, D. Schubert, N. Demmel, D. Cremers, In 2020 Conference on Computer Vision and Pattern Recognition (CVPR), [[DOI:10.1109/CVPR42600.2020.01116]](https://doi.org/10.1109/CVPR42600.2020.01116), [[arXiv:1911.08860]](https://arxiv.org/abs/1911.08860).

If you are planning to use the code in your project check the `include/basalt/spline` folder of the [header-only library](https://gitlab.com/VladyslavUsenko/basalt-headers) ([Documentation](https://vladyslavusenko.gitlab.io/basalt-headers/)).

For a calibration tool based on the proposed B-spline trajectory representation check the [dataset](https://gitlab.com/VladyslavUsenko/basalt/blob/master/doc/Calibration.md) and [device](https://gitlab.com/VladyslavUsenko/basalt/blob/master/doc/Realsense.md) calibration tutorials of the [basalt](https://gitlab.com/VladyslavUsenko/basalt) project.

## Installation for Ubuntu 18.04 and MacOS
Clone the source code for the project and build it. For MacOS you should have [Homebrew](https://brew.sh/) installed.
```
git clone --recursive https://gitlab.com/tum-vision/lie-spline-experiments.git
cd lie-spline-experiments
./install_dependencies.sh
./build_submodules.sh
mkdir build
cd build
cmake .. -DCMAKE_BUILD_TYPE=Release
make -j8
ctest              # run tests to make sure both formulations produce the same result
./eval_lie_spline  # run simulation experiments
./eval_calib       # run calibration experiments
```

## Licence
The code is provided under a BSD 3-clause license. See the LICENSE file for details.
Note also the different licenses of thirdparty submodules.
